

---

TeamEverywhere NestJS Guide
*Updated at 2022-09-06*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
*generated with [TeamEverywhere](https://www.team-everywhere.com/)*

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Authentication
  - Authentication은 대부분의 앱에서 가장 중요한 부분 중 하나 입니다.
  - 인증을 처리하는 방법에는 다양한 전략과 접근 방법이 있습니다.

## Passport
  - Passport는 nodejs 생태계에서 가장 유명한 인증 라이브러리 입니다. 
  - NestJS에서는 passport 라이브러리를 활용하기 위한 @nestjs/passport를 제공합니다.
  - Passport는 다음과 같은 절차에 의해 인증을 실행합니다.
    - JWT와 같은 credentials을 검증합니다.
    - token 등을 발행하여 인증 상태를 관리 합니다.
    - Request object에 인증된 유저의 정보를 추가하여 route handler에서 확인할 수 있게 합니다. 
  - 자 그럼 이제 유저의 username과 password를 가고 인증한다고 생각해 봅시다. 
  - 한번 인증이 되면 서버는 JWT를 발행할 것이고, 클라이언트는 이 JWT 토큰을 bearer token으로 header에 전달하게 됩니다.
  - 그리고 API에서는 해당 bearer token과 같이 요청해야만 제대로 된 응답을 받을 수 있는 end point를 설정하여 인증 관리를 하게 됩니다.
  - 실습을 위해 아래와 같이 필요 모듈을 설치해 봅시다.
  ```bash
    npm install --save @nestjs/passport passport passport-local
    npm install --save-dev @types/passport-local
  ```

  # 인증 구현 
    - NestJS 스타일로 적용된 @nestjs/passport 모듈을 활용하기 전에 원래의 passport를 한번 살펴보겠습니다.
    - vanila passport 모듈을 사용하면, 아래와 같은 방식들로 인증을 구현할 수 있습니다.
      - JWT strategy 내에 여러 옵션을 설정하여 인증을 처리
      - verify callback을 사용해서 유저의 존재 여부, 인증 여부 등을 처리
    - @nestjs/passport 모듈을 사용하면 PassportStrategy 클래스를 상속받음으로써 strategy 설정을 할 수 있습니다.
    - super() 메소드를 통해 각종 strategy option을 전달합니다.
    - 또한 validate() 메소드를 통해 verify callback을 구현할 수 있습니다.
    - 우선은 auth module과 auth service를 만드는 것으로 시작해 보겠습니다.
```bash
    $ nest g module auth
    $ nest g service auth
```
  
  - user service 코드에 아래와 같은 예제 코드를 추가해보겠습니다.

```typescript
  private readonly users = [
    {
      userId: 1,
      username: 'john',
      password: 'changeme',
    },
    {
      userId: 2,
      username: 'maria',
      password: 'guess',
    },
  ];

  async findOne(username: string): Promise<User | undefined> {
    return this.users.find(user => user.username === username);
  }
```

   - auth service 파일의 코드를 다음과 같이 수정해보겠습니다.

```typescript

  import { Injectable } from '@nestjs/common';
  import { UsersService } from '../users/users.service';

  @Injectable()
  export class AuthService {
    constructor(private usersService: UsersService) {}

    async validateUser(username: string, pass: string): Promise<any> {
      const user = await this.usersService.findOne(username);
      if (user && user.password === pass) {
        const { password, ...result } = user;
        return result;
      }
      return null;
    }
  }
```
  - validateUser()라는 메소드를 만들어서 유저와 verified password를 전달받습니다.
  - validateUser() 메소드는 Passport local strategy에서 호출할 예정입니다.
  - auth module에 아래와 같이 UsersModule을 import해 줍니다.

```typescript
  import { Module } from '@nestjs/common';
  import { AuthService } from './auth.service';
  import { UsersModule } from '../users/users.module';

  @Module({
    imports: [UsersModule],
    providers: [AuthService],
  })
  export class AuthModule {}

```

## Passport local
  - 이제 passport의 local authentication strategy를 구축하겠습니다.
  - auth 폴더 내에 local.strategy.ts 파일을 생성하겠습니다.

```typescript
  import { Strategy } from 'passport-local';
  import { PassportStrategy } from '@nestjs/passport';
  import { Injectable, UnauthorizedException } from '@nestjs/common';
  import { AuthService } from './auth.service';

  @Injectable()
  export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(private authService: AuthService) {
      super();
    }

    async validate(username: string, password: string): Promise<any> {
      const user = await this.authService.validateUser(username, password);
      if (!user) {
        throw new UnauthorizedException();
      }
      return user;
    }
  }
```

  - 위 코드에서 우리는 validate() 메소드를 정의했습니다.
  - Passport는 validate()로 명명한 검증 코드를 호출할 것입니다. 
  - 해당 검증 코드는 간단하게 유저가 존재하지 않을 경우 UnauthorizedException 에러를 발생시킵니다.
  - 일반적으로 validate 코드내에서 strategy 별로 다르게 작업하는 내용은 어떻게 유저를 검증하여 유효한지를 체크하는 코드라고 보시면 됩니다.
  - 자 이제 아래와 같이 AuthModule내에 passport 관련 설정들을 추가해줍시다.

```typescript
  import { Module } from '@nestjs/common';
  import { AuthService } from './auth.service';
  import { UsersModule } from '../users/users.module';
  import { PassportModule } from '@nestjs/passport';
  import { LocalStrategy } from './local.strategy';

  @Module({
    imports: [UsersModule, PassportModule],
    providers: [AuthService, LocalStrategy, UsersService]
  })
  export class AuthModule {}
```

  - 프로바이더 참조 에러가 나지 않도록 아래와 같이 user module 에서 repository도 export 해줍시다.

```typescript
  import { Module } from '@nestjs/common';
  import { TypeOrmModule } from '@nestjs/typeorm';
  import { UsersController } from './users.controller';
  import { User } from './users.entity';
  import { UsersService } from './users.service';
  import { UserSubscriber } from './users.subscriber';

  @Module({
    imports: [TypeOrmModule.forFeature([User])],
    exports: [TypeOrmModule],
    controllers: [UsersController],
    providers: [
      UsersService, 
      // UserSubscriber
    ]
  })
  export class UsersModule {}
```

## Built-in Passport Guards
  - 자 그럼 이제 passport의 built in guard를 활용해서 유저가 로그인 한 경우와 로그인 하지 않은 경우를 체크해보겠습니다.
  - 유저가 로그인하지 않은 경우, 특정 route에 접근 하는 것을 제한할 수 있습니다.
  - Guard를 권한이 필요한 route에 위치시켜 보호할 수 가 있습니다.

## Login route
  - 이제 passport의 built-in Guard를 적용해서 Controller에 추가해서 테스트해보겠습니다.

```typescript
    @UseGuards(AuthGuard('local'))
    @Post('auth/login')
    async login(@Request() req) {
      return req.user;
    }  
```

  - 위 코드를 보시면 login 함수안에 user를 가져오는 어떠한 코드도 없지만 Guard를 활용해서 passport-local stratgy를 한번 참조하고 라우트가 호출 되기 때문에, 유저 정보를 전달받을 수 있는 것을 확인할 수 있습니다.
  - 위 코드가 작동한다면 AuthGuard('local')를 제대로 string 베이스로 호출할 수도 있습니다.
  - 아래와 같은 파일을 auth/local-auth.guard.ts내에 만듭시다.

```typescript
  import { Injectable } from '@nestjs/common';
  import { AuthGuard } from '@nestjs/passport';

  @Injectable()
  export class LocalAuthGuard extends AuthGuard('local') {}
```

  - 그리고 아래와 같이 Guard 코드를 변경할 수 있습니다.

```typescript
  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  async login(@Request() req) {
    return req.user;
  }
```
  
  - 자 이제 passport와 built-in guard를 활용한 validate 코드의 기본적인 작동 방법을 알게 되었습니다.

## JWT 
  - 이제 JWT를 활용해서 인증 시스템에 적용 해보겠습니다.
  - 우리는 인증이 된 유저에게 JWT를 응답해서 이후 API에서 검증하여 사용하도록 하겠습니다. 
  - 그리고 이제 클라이언트는 bearer token 방식으로 API에 전달해야만 해당 API에 접근할 수 있도록 할 예정입니다.
  - 아래와 같이 추가 모듈을 설치합시다.

```bash
  $ npm install --save @nestjs/jwt passport-jwt
  $ npm install --save-dev @types/passport-jwt
```

  - @nestjs/jwt 모듈은 JWT 조작을 도와주는 모듈입니다.
  - passport-jwt 모듈은 JWT strategy를 구축할 수 있게 해주는 모듈입니다.
  - @types/passport-jwt는 TypeScript 정의를 제공합니다.
  - POST user/auth/login 코드는 built-in AuthGuard를 활용해서 유저가 제대로 인증될 때만 해당 라우트가 실행되게 작업하였습니다.
  - 또한 req 파라미터는 user 객체를 가지게 됩니다.
  - 자 이제 JWT를 생성하는 코드를 아래와 같이 추가하겠습니다.

```typescript
  import { Injectable } from '@nestjs/common';
  import { UsersService } from '../users/users.service';
  import { JwtService } from '@nestjs/jwt';

  @Injectable()
  export class AuthService {
    constructor(
      private usersService: UsersService,
      private jwtService: JwtService
    ) {}

    async validateUser(username: string, pass: string): Promise<any> {
      const user = await this.usersService.findOne(username);
      if (user && user.password === pass) {
        const { password, ...result } = user;
        return result;
      }
      return null;
    }

    async login(user: any) {
      const payload = { username: user.username, sub: user.user_idx };
      return {
        access_token: this.jwtService.sign(payload),
      };
    }
  }
```

  - @nestjs/jwt의 sign 함수는 payload로 전달하는 데이터(여기서는 User)를 기준으로 JWT를 생성합니다.
  - 그리고 이제 auth module에 JWT module을 Import 하겠습니다.
  - 우선 auth 폴더 내에 constants.ts 파일을 만들도록 하겠습니다.

```typescript
  export const jwtConstants = {
    secret: 'secretKey',
  };
```

  - 위 키를 JWT 인증과, 검증 단계에서 사용할 예정입니다.
  - 상용서버에서는 이 키가 노출이 안되도록 작업을 하셔야 된다는 점 꼭 기억 하시기 바랍니다.
  - auth.module.ts의 코드를 아래와 같이 변경합시다.

```typescript
  import { Module } from '@nestjs/common';
  import { AuthService } from './auth.service';
  import { LocalStrategy } from './local.strategy';
  import { UsersModule } from '../users/users.module';
  import { PassportModule } from '@nestjs/passport';
  import { JwtModule } from '@nestjs/jwt';
  import { jwtConstants } from './constants';

  @Module({
    imports: [
      UsersModule,
      PassportModule,
      JwtModule.register({
        secret: jwtConstants.secret,
        signOptions: { expiresIn: '60s' },
      }),
    ],
    providers: [AuthService, LocalStrategy],
    exports: [AuthService],
  })
  export class AuthModule {}
```

  - JwtModule의 register() 메소드를 활용해서 설정 객체를 전달하여 등록합니다.
  - 이제 user/auth/login route의 코드를 다음과 같이 변경하겠습니다.

```typescript
    @UseGuards(LocalAuthGuard)
    @Post('auth/login')
    async login(@Request() req) {
      return this.authService.login(req.user);
    }  
```
  
  - user module에도 jwt 설정을 해줍시다.
  - 자 이제 access token을 문제없이 받아오는 지 확인해봅시다.

## Passport JWT
  - 자 이제 route를 보호하도록 합시다.
  - 특정 route들은 JWT가 같이 전달되어야 접근이 가능하도록 만들겠습니다.
  - 아래와 같이 auth 폴더에 jwt.strategy.ts를 만들도록 합시다.

```typescript
  import { ExtractJwt, Strategy } from 'passport-jwt';
  import { PassportStrategy } from '@nestjs/passport';
  import { Injectable } from '@nestjs/common';
  import { jwtConstants } from './constants';

  @Injectable()
  export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor() {
      super({
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        ignoreExpiration: false,
        secretOrKey: jwtConstants.secret,
      });
    }

    async validate(payload: any) {
      return { user_idx: payload.sub, username: payload.username };
    }
  }
```

  - 위 코드는 super 메소드에서 몇 가지 초기화를 해줄 필요가 있습니다.
  - jwtFromRequest 
    - request에서 JWT를 추출하는 방식을 나타냅니다.
    - 우리는 일반적인 방법 중 하나인 BearerToken 을 Header에서 추출하는 방법을 사용할 것 입니다.
  - ignoreExpiration
    - false로 했기 때문에 만료된 JWT를 받을 경우 Unauthorized response를 응답합니다.
  - secretOrKey
    - token 인증 및 발급에 쓰일 signing key 입니다.
  - validate() 메소드에서는 우선 JWT를 decode 하여 payload를 파라미터로 받을 수 있습니다.
  - JWT의 인증방식을 통해 우리는 적합한 토큰을 전달받은 적합한 유저인지를 검증합니다.
  - 새로운 JWT Strategy를 module에 추가해주겠습니다.
  
```typescript
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { UsersModule } from 'src/users/users.module';
import { UsersService } from 'src/users/users.service';
import { AuthService } from './auth.service';
import { jwtConstants } from './constants';
import { JwtStrategy } from './jwt.strategy';
import { LocalStrategy } from './local.strategy';

@Module({  
  imports: [
    UsersModule,
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '60s' },
    })],
  providers: [AuthService, LocalStrategy, UsersService, JwtStrategy],
  exports: [AuthService],
})
export class AuthModule {}
```


  - auth 폴더 내에 JwtAuthGuard 파일을 만들어 주겠습니다.

```typescript
  import { Injectable } from '@nestjs/common';
  import { AuthGuard } from '@nestjs/passport';

  @Injectable()
  export class JwtAuthGuard extends AuthGuard('jwt') {}
```


## protected route and JWT strategy guards
  - 자 이제 JWT strategy를 적용하여 우리의 route를 보호해보겠습니다.
  - 다음과 같은 코드를 user controller 파일에 추가합니다.

```typescript
  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@Request() req) {
    return req.user;
  }
```

  - 이제 인증 후 bearer token으로 해당 api endpoint를 호출해보겠습니다.



----

