

---

TeamEverywhere NestJS Guide
*Updated at 2022-09-06*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
*generated with [TeamEverywhere](https://www.team-everywhere.com/)*

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Modules
<img src="Modules_1.png" width="1280" height="540" />

  - @Module() 데코레이터가 있으면 NestJS는 모듈로 인식하고, 어플리케이션의 구조를 조직하는 데 활용합니다.
  - 어플리케이션은 최소한 적어도 하나의 모듈이 필요합니다. 
  - root module은 어플리케이션의 구조가 시작되는 지점으로, 의존성 및 프로바이더들의 관계를 구성하는 시작점이 됩니다.
  - module은 단 하나만 있어도 되지만 NestJS는 아키텍쳐 구성에 따라 여러 모듈을 구성하고 캡슐화 하는 것을 권장합니다.
  - 모듈의 구성요소는 다음과 같습니다.
|providers|모듈 간 공유가 가능한 의존성 요소들|
|controllers|해당 모듈 내에서 정의되고 객체화 되어야 하는 컨트롤러들|
|imports|해당 모듈에서 임포트 하는 프로바이더들|
|exports|해당 모듈에서 제공되는 프로바이더들 중 다른 모듈에서 임포트하여 사용 가능한 것들|
  - 모듈은 프로바이더들을 캡슐화 하기 때문에 exports 하기 전엔 다른 모듈에서 사용할 수 없습니다.

## Feature modules
  - Feature module은 특정한 기능, 구조를 위해 작성한 코드들을 관리하기 위한 모듈입니다.
  - 명확한 기능의 범위를 지정하고, 보다 복잡하고 거대한 어플리케이션을 만드는 데 활용합니다.
  - 아래와 같이 CatsModule은 Feature Module이라고 할 수 있습니다.
  ```typescript
  import { Module } from '@nestjs/common';
  import { CatsController } from './cats.controller';
  import { CatsService } from './cats.service';

  @Module({
    controllers: [CatsController],
    providers: [CatsService],
  })
  export class CatsModule {}
  ```
  - 각각의 모듈은 root module 내에 임포트 되어야 합니다.
  ```typescript
  import { Module } from '@nestjs/common';
  import { CatsModule } from './cats/cats.module';

  @Module({
    imports: [CatsModule],
  })
  export class AppModule {}
  ```

## Shared modules
  - NestJS에서 각각의 모듈은 기본적으로 singleton 입니다.
  - 때문에 각각의 모듈들을 share해서 사용할 수 있습니다.
  <img src="Shared_Module_1.png" width="1280" height="540" />
  - 이런 특성을 활용하여, CatsService를 여러 모듈에서 사용하게 하려면 아래와 같이 export 해주어야 합니다.
  - 이렇게 하면 CatsModule을 import하는 모듈은 모두 CatsService를 사용할 수 있게 됩니다.
  ```typescript
  import { Module } from '@nestjs/common';
  import { CatsController } from './cats.controller';
  import { CatsService } from './cats.service';

  @Module({
    controllers: [CatsController],
    providers: [CatsService],
    exports: [CatsService]
  })
  export class CatsModule {}
  ```

## Module re-exporting
  - 각각의 모듈은 import한 모듈을 다시 export할 수도 있습니다.
  ```typescript
  @Module({
    imports: [CommonModule],
    exports: [CommonModule],
  })
  export class CoreModule {}
  ```

## Dependency injection
  - 설정등의 이유로 활용하기 위해, 모듈은 생성자를 활용해 의존성을 주입 받을 수도 있습닏.
  ```typescript
    import { Module } from '@nestjs/common';
    import { CatsController } from './cats.controller';
    import { CatsService } from './cats.service';

    @Module({
      controllers: [CatsController],
      providers: [CatsService],
    })
    export class CatsModule {
      constructor(private catsService: CatsService) {}
    }
  ```
  - 하지만 모듈 클래스들은 provider로 취급되어 주입될 수 없습니다.


## Global modules
  - NestJS는 provider들이 module 내에서만 활용될 수 있도록 캡슐화 합니다.
  - 때문에 캡슐화된 모듈을 import하기 전에는 다른 module에서 다른 module의 provider를 사용할 수 없습니다.
  - 만약 특정 provider들을 어디서든 사용 가능하게 만들고 싶다면, 모듈을 global화 해야 합니다.
  - @Global() 데코레이터로 global화 하는 모듈은 단 한번만 등록 되어야 합니다.(일반적으로는 root module 혹은 core module로 활용)
  - 이렇게 하면 이제 CatsModule을 import 하지 않고도 CatsModule 내 서비스들을 다른 module에서 사용할 수 있습니다.
  ```typescript
  import { Module, Global } from '@nestjs/common';
  import { CatsController } from './cats.controller';
  import { CatsService } from './cats.service';

  @Global()
  @Module({
    controllers: [CatsController],
    providers: [CatsService],
    exports: [CatsService],
  })
  export class CatsModule {}
  ```

## Dynamic modules
  - 아래와 같이 Custom Module을 생성하여 사용할 수도 있습니다.
  ```typescript
  import { Module, DynamicModule } from '@nestjs/common';
  import { createDatabaseProviders } from './database.providers';
  import { Connection } from './connection.provider';

  @Module({
    providers: [Connection],
  })
  export class DatabaseModule {
    static forRoot(entities = [], options?): DynamicModule {
      const providers = createDatabaseProviders(options, entities);
      return {
        module: DatabaseModule,
        providers: providers,
        exports: providers,
      };
    }
  }
  ```
  


----

