

---

TeamEverywhere NestJS Guide
*Updated at 2022-09-06*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
*generated with [TeamEverywhere](https://www.team-everywhere.com/)*

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## Relations
  - Relation은 두개 이상의 테이블간에 성립된 관계를 의미합니다.
  - Relation의 관계는 아래와 같이 세개의 타입으로 구성됩니다.

|Relation의|Description|
|-------|----------|
|One-to-one|하나의 row가 하나의 row하고만 연결됩니다. |
|One-to-many / Many-to-one|하나의 row가 하나 이상의 row와 연결 됩니다. |
|autoLoadEntities|모든 row가 다수의 row와 연결될 수 있습니다.|

  - Relation을 사용하기 위해서는 아래 코드와 같이 데코레이터를 사용해 정의하면 됩니다.
  - User 여러개의 photos를 가질 수 있도록 정의해봅시다.
```typescript
  import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
  import { Photo } from '../photos/photo.entity';

  @Entity()
  export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column({ default: true })
    isActive: boolean;

    @OneToMany(type => Photo, photo => photo.user)
    photos: Photo[];
  }
```

## Cascade
  - Cascade는 true로 되어 있다면, insert와 update시 연관된 데이터가 같이 적용 됩니다.
```typescript
  // Category Entity
  import { Entity, PrimaryGeneratedColumn, Column, ManyToMany } from "typeorm"
  import { Question } from "./Question"

  @Entity()
  export class Category {
      @PrimaryGeneratedColumn()
      id: number

      @Column()
      name: string

      @ManyToMany((type) => Question, (question) => question.categories)
      questions: Question[]
  }
  

  // Question entity
  import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToMany,
    JoinTable,
  } from "typeorm"
  import { Category } from "./Category"

  @Entity()
  export class Question {
      @PrimaryGeneratedColumn()
      id: number

      @Column()
      title: string

      @Column()
      text: string

      @ManyToMany((type) => Category, (category) => category.questions, {
          cascade: true,
      })
      @JoinTable()
      categories: Category[]
  }
  // code 
  const category1 = new Category()
  category1.name = "ORMs"

  const category2 = new Category()
  category2.name = "Programming"

  const question = new Question()
  question.title = "How to ask questions?"
  question.text = "Where can I ask TypeORM-related questions?"
  question.categories = [category1, category2]
  await this.questionRepository.manager.save(question)
```

  - 위 코드를 보시면 우리는 category1과 category2를 위핸 save를 메소드를 호출하지 않았음에도 테이블에 저장이 된 것을 확인할 수 있습니다.
  - 왜냐면 cascade를 true로 지정했기 때문이죠.
  - 혹은 아래처럼 array 형태로 원하는 cascade만 지정해줄 수 있습니다.

```typescript
  @Entity(Post)
  export class Post {
      @PrimaryGeneratedColumn()
      id: number

      @Column()
      title: string

      @Column()
      text: string

      // Full cascades on categories.
      @ManyToMany((type) => PostCategory, {
          cascade: true,
      })
      @JoinTable()
      categories: PostCategory[]

      // PostDetail 인스턴스 셋이 있을경우, insert 시 자동으로 같이 저장 됩니다.
      @ManyToMany((type) => PostDetails, (details) => details.posts, {
          cascade: ["insert"],
      })
      @JoinTable()
      details: PostDetails[]

      // Post update tl, PostImage의 업데이트가 있을경우, 해당 테이블도 자동으로 업데이트 됩니다.
      @ManyToMany((type) => PostImage, (image) => image.posts, {
          cascade: ["update"],
      })
      @JoinTable()
      images: PostImage[]
      

      // Insert와 Update 모두 자동으로 연동 됩니다.
      @ManyToMany((type) => PostInformation, (information) => information.posts, {
          cascade: ["insert", "update"],
      })
      @JoinTable()
      informations: PostInformation[]
  }
```

## @JoinColumn options
  - JoinColumn이 있을 경우 join table의 컬럼명을 자동으로 생성합니다.
  - 예를 들어, 아래와 같은 코드가 있을 때 생성되는 컬럼의 규칙은 propertyName + referencedColumnName가 됩니다.
  - 즉, categoryId라는 컬럼명을 만들어 냅니다.
```typescript
    @ManyToOne(type => Category)
    @JoinColumn() // this decorator is optional for @ManyToOne, but required for @OneToOne
    category: Category;
```
  - 혹은, 아래와 같이 이름을 지어줄 수가 있습니다.
```typescript
  @ManyToOne(type => Category)
  @JoinColumn({ name: "cat_id" })
  category: Category;
```


----

