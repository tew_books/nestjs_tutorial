

---

TeamEverywhere NestJS Guide
*Updated at 2022-09-06*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
*generated with [TeamEverywhere](https://www.team-everywhere.com/)*

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Middleware
  - middleware는 route handler가 호출되기 전에 호출되는 함수 입니다.
  - middleware는 request와 response 객체에 접근 및 수정할 수 있습니다.
  - next() 미들웨어를 활용해 stack으로 구성할 수 있습니다.
  - middleware를 만들려면 반드시 NestMiddleware interface를 상속해야 합니다.
  - 아래와 같이 middleware 클래스 코드를 살펴 봅시다.
  - 아래 미들웨어에서 use 함수의 인수들을 전달 받아 작업할 수 있습니다.
  - 또한 next()를 호출하여 작업 후 다음 미들웨어가 호출이 되도록 설정해주었습니다.
```typescript
    import { Injectable, NestMiddleware } from '@nestjs/common';
    import { Request, Response, NextFunction } from 'express';

    @Injectable()
    export class LoggerMiddleware implements NestMiddleware {
      use(req: Request, res: Response, next: NextFunction) {
        console.log('Request...');
        next();
      }
    }
```

## Applying middleware
 - Module에는 middleware를 위한 적용 함수가 없습니다.
 - 대신 모듈의 configure() 함수를 활용하여 middleware를 적용할 수 있습니다.
 - middleware를 사용하는 module 클래스는 NestModule interface를 상속해야 합니다.
 - 아래 코드와 같이 middleware의 적용 route도 설정해줄 수 있습니다.
 ```typescript
  import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
  import { LoggerMiddleware } from './common/middleware/logger.middleware';
  import { CatsModule } from './cats/cats.module';

  @Module({
    imports: [CatsModule],
  })
  export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
      consumer
        .apply(LoggerMiddleware)
        .forRoutes('cats');
    }
  }
 ```
 - 아래와 같이 request method도 정할 수 있습니다.
 ```typescript
  import { Module, NestModule, RequestMethod, MiddlewareConsumer } from '@nestjs/common';
  import { LoggerMiddleware } from './common/middleware/logger.middleware';
  import { CatsModule } from './cats/cats.module';

  @Module({
    imports: [CatsModule],
  })
  export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
      consumer
        .apply(LoggerMiddleware)
        .forRoutes({ path: 'cats', method: RequestMethod.GET });
    }
  }
 ```
 
## Middleware consumer
  - middleware consumer는 헬퍼 클래스 입니다.
  - consumer는 middleware를 관리하기 위한 몇가지 메소드를 제공합니다.
  - forRoutes() 는 single string, multiple string , RouteInfo Object, multiple controller 등 다양한 타입의 파라미터를 지원합니다.

## Excluding routes#
  - exclude 메소드를 사용해서 middleware를 특정 routes에 적용할 수도 있습니다.
  - exclude()는 wildcard parameters를 지원합니다.
  ```typescript
    consumer
    .apply(LoggerMiddleware)
    .exclude(
      { path: 'cats', method: RequestMethod.GET },
      { path: 'cats', method: RequestMethod.POST },
      'cats/(.*)',
    )
    .forRoutes(CatsController);
  ```

## Functional middleware
  - LoggerMiddleware class 같이 간단한 클래스는 함수형으로 간단하게 구축해도 문제가 없을 겁니다.
  ```typescript
    // logger.middleware.ts
    import { Request, Response, NextFunction } from 'express';

    export function logger(req: Request, res: Response, next: NextFunction) {
      console.log(`Request...`);
      next();
    };

    // app.module.ts
    consumer
    .apply(logger)
    .forRoutes(CatsController);
  ```

## Multiple middleware
  - 여러개의 middleware를 적용하려면 다음과 같이 콤마로 구분하여 적용하면 됩니다.
  ```typescript
    consumer.apply(cors(), helmet(), logger).forRoutes(CatsController);

  ```

## Global middleware
  - INestApplication instance의 use 메소드를 이용하여 middleware를 등록하면 global로 사용이 가능합니다.
  ```typescript
    const app = await NestFactory.create(AppModule);
    app.use(logger);
    await app.listen(3000);
```


----

