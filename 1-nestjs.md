
---

TeamEverywhere NestJS Guide
*Updated at 2022-09-06*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
*generated with [TeamEverywhere](https://www.team-everywhere.com/)*

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## 정의
  - NestJS는 scalable한 NodeJS의 server-side application을 만들기 위한 프레임워크 입니다.
  - NestJS는 Typescript를 지원하여 저희는 Typescript 기반으로 프로젝트를 작업할 수 있습니다.
  - 또한 NestJS는 OOP, FP, FRP(Functional Reactive Programming)를 모두 지원합니다.
  - NestJS는 Express 혹은 Fastify 프레임워크의 설정을 기반으로 작업할 수 있습니다.

## 철학
  - NodeJS의 발전으로 여러가지 프레임워크들이 생겨났지만 -Angular, React, Vue-, 
    중요한 문제 하나를 해결하지 못했습니다.
  - NestJS는 highly testable, scalable, loosely coupled, and easily maintainable한
    아키텍쳐를 제공하는 데 그 목적이 있습니다.

## 설치
  - npm i -g @nestjs/cli
  - nest new project-name

## 기본 구조
  - app.controller.ts	: 라우트의 컨트롤러
  - app.controller.spec.ts: 컨트롤러 단위 테스트 코드
  - app.module.ts	: 앱의 루트 모듈
  - app.service.ts: 여러 로직을 담는 파일
  - main.ts	: 앱의 시작 파일

## main.ts
  - create 메소드는 INestApplication interface에서 application object를 반환합니다.
  - 내장 http listener 코드를 활용하여 application object가 inbound http request를 받을 수 있도록 합니다. 

----
