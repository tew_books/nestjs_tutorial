

---

TeamEverywhere NestJS Guide
*Updated at 2022-09-06*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
*generated with [TeamEverywhere](https://www.team-everywhere.com/)*

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Authorization
  - Authorization은 어떤 유저가 어떠한 권한을 가질 지를 결정합니다.
  - 예를 들어 관리자 유저는 데이터의 생성, 조회, 수정, 삭제 등을 모두 할 수 있지만, 비 관리자 유저는 읽기만 가능한 것이죠.

## Basic RBAC implementation
  - 우리는 Guards를 활용해 기본적인 Role-based access control (RBAC) 메커니즘을 구현할 예정입니다.
  - 아래와 같은 role을 만들겠습니다. (물론 실제 앱에서는 role을 데이터베이스 등에 저장하는 경우가 많을 것입니다.)

```typescript
  export enum Role {
    User = 'user',
    Admin = 'admin',
  }
```

  - 위 코드를 활용해 @Roles() Decorator를 만들겠습니다.
  - 해당 데코레이터를 통해 특정 자원에 접근하기 위해 어떠한 role이 필요한 지 정의합니다.

```typescript
  import { SetMetadata } from '@nestjs/common';
  import { Role } from '../enums/role.enum';

  export const ROLES_KEY = 'roles';
  export const Roles = (...roles: Role[]) => SetMetadata(ROLES_KEY, roles);
```

  - 이제 다음과 같이 custom decorator를 추가해주겠습니다.

```typescript
    @Post()
    @Roles(Role.Admin)
    create(@Body() createGoodDto: CreateGoodDto): Promise<InsertResult> {
      return this.goodsService.create(createGoodDto);
    }
```

  - 이제 RolesGuard를 만들어서 현재 유저에 부여된 권한으로 해당 라우터에 접근할 수 있는 지 확인하는 코드를 만들겠습니다.
  - 우선 User class에 roles를 추가하겠습니다.

```typescript
  class User {
    // ...other properties
    roles: Role[];
  }
```

  - 이에 맞게 관련 코드를 수정해주고 아래와 같이 RolesGuard를 설정해줍니다.

```typescript
  @Post()  
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  create(@Body() createGoodDto: CreateGoodDto): Promise<InsertResult> {
    return this.goodsService.create(createGoodDto);
  }

  //jwt strategy도 roles를 가져올 수 있게 아래 코드를 추가해줍시다.
    async validate(payload: any) {
    // get user role
    const user = await this.usersService.findOne(payload.username);
    return { user_idx: payload.sub, username: payload.username, roles:user.roles };
  }
```
  
  - 자 그럼 이제 테스트를 해보면 role에 맞게 사용할 수 있는 것을 알 수 있습니다.
  


----

