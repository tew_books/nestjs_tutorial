

---

TeamEverywhere NestJS Guide
*Updated at 2022-09-06*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
*generated with [TeamEverywhere](https://www.team-everywhere.com/)*

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Auto-load entities
  - 매번 entity들을 모듈의 datasource에 등록하는 것은 번거로운 일입니다.
  - 또한 entity를 수동으로 추가할 경우, 어플리케이션의 도메인 범위를 망가뜨리거나 다른 부분에 문제를 일으킬 가능성이 있습니다.
  - 때문에 autoLoadEntities 설정을 통해 entity들을 추가하는 것을 권합니다. 
  - 이때 에러가 나면 각 모듈의 forFeature 메소드에서 연관된 entity들을 등록해주어야 합니다.
  
## Transactions
  - 트랜지액션은 데이터베이스의 데이터들을 관리하는 일련의 작업들을 의미합니다.
  - 일반적으로는 데이터상에서 일련의 변화를 일으키는 것들을 의미하기도 합니다.
  - TypeORM은 여러가지 트랜지액션 처리 방법이 있는데 NestJS는 QueryRunner를 사용하는 것을 권장합니다.
  - QueryRunner는 가능한 모든 제어 방법을 제공하기 때문입니다.
  - 우선 아래와 같이 datasource를 주입하여야 합니다.
  ```typescript
  @Injectable()
  export class UsersService {
    constructor(private dataSource: DataSource) {}
  }
  ```
  - 그 다음에는 transaction을 위한 object를 만듭니다.
  ```typescript
    async createMany(users: User[]) {
    const queryRunner = this.dataSource.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      await queryRunner.manager.save(users[0]);
      await queryRunner.manager.save(users[1]);

      await queryRunner.commitTransaction();
    } catch (err) {
      // since we have errors lets rollback the changes we made
      await queryRunner.rollbackTransaction();
    } finally {
      // you need to release a queryRunner which was manually instantiated
      await queryRunner.release();
    }
  }
  ```
  - 혹은 아래와 같이 콜백 스타일로 구성할 수도 있습니다.
  ```typescript
  async createMany(users: User[]) {
    await this.dataSource.transaction(async manager => {
      await manager.save(users[0]);
      await manager.save(users[1]);
    });
  }
  ```
  - 트랜지액션을 구축하기 위해 데코레이터를 사용하는 것은 권장되지 않습니다.


## Subscribers
  - typeorm subscriber를 통해 특정 엔티티 이벤트를 받을 수 있습니다.

```typescript
  import {
    DataSource,
    EntitySubscriberInterface,
    EventSubscriber,
    InsertEvent,
    RecoverEvent,
    RemoveEvent,
    SoftRemoveEvent,
    TransactionCommitEvent,
    TransactionRollbackEvent,
    TransactionStartEvent,
    UpdateEvent,
  } from 'typeorm';
  import { User } from './users.entitiy';

  @EventSubscriber()
  export class UserSubscriber implements EntitySubscriberInterface<User> {
    constructor(dataSource: DataSource) {
      dataSource.subscribers.push(this);
    }

    listenTo() {
      return User;
    }

    afterLoad(entity: any) {
      console.log(`AFTER ENTITY LOADED: `, entity)
    }

    beforeInsert(event: InsertEvent<any>) {
        console.log(`BEFORE POST INSERTED: `, event.entity)
    }

    afterInsert(event: InsertEvent<any>) {
        console.log(`AFTER ENTITY INSERTED: `, event.entity)
    }

    beforeUpdate(event: UpdateEvent<any>) {
        console.log(`BEFORE ENTITY UPDATED: `, event.entity)
    }

    afterUpdate(event: UpdateEvent<any>) {
        console.log(`AFTER ENTITY UPDATED: `, event.entity)
    }

    beforeRemove(event: RemoveEvent<any>) {
        console.log(
            `BEFORE ENTITY WITH ID ${event.entityId} REMOVED: `,
            event.entity,
        )
    }

    afterRemove(event: RemoveEvent<any>) {
        console.log(
            `AFTER ENTITY WITH ID ${event.entityId} REMOVED: `,
            event.entity,
        )
    }

    beforeSoftRemove(event: SoftRemoveEvent<any>) {
        console.log(
            `BEFORE ENTITY WITH ID ${event.entityId} SOFT REMOVED: `,
            event.entity,
        )
    }

    afterSoftRemove(event: SoftRemoveEvent<any>) {
        console.log(
            `AFTER ENTITY WITH ID ${event.entityId} SOFT REMOVED: `,
            event.entity,
        )
    }

    beforeRecover(event: RecoverEvent<any>) {
        console.log(
            `BEFORE ENTITY WITH ID ${event.entityId} RECOVERED: `,
            event.entity,
        )
    }

    afterRecover(event: RecoverEvent<any>) {
        console.log(
            `AFTER ENTITY WITH ID ${event.entityId} RECOVERED: `,
            event.entity,
        )
    }

    beforeTransactionStart(event: TransactionStartEvent) {
        console.log(`BEFORE TRANSACTION STARTED: `, event)
    }

    afterTransactionStart(event: TransactionStartEvent) {
        console.log(`AFTER TRANSACTION STARTED: `, event)
    }

    beforeTransactionCommit(event: TransactionCommitEvent) {
        console.log(`BEFORE TRANSACTION COMMITTED: `, event)
    }

    afterTransactionCommit(event: TransactionCommitEvent) {
        console.log(`AFTER TRANSACTION COMMITTED: `, event)
    }

    beforeTransactionRollback(event: TransactionRollbackEvent) {
        console.log(`BEFORE TRANSACTION ROLLBACK: `, event)
    }

    afterTransactionRollback(event: TransactionRollbackEvent) {
        console.log(`AFTER TRANSACTION ROLLBACK: `, event)
    }
  } 
```
  
  - 위 이벤트 subscriber를 providers에 추가해봅시다.

```typescript
  import { Module } from '@nestjs/common';
  import { TypeOrmModule } from '@nestjs/typeorm';
  import { UsersController } from './users.controller';
  import { User } from './users.entitiy';
  import { UsersService } from './users.service';
  import { UserSubscriber } from './users.subscriber';

  @Module({
    imports: [TypeOrmModule.forFeature([User])],
    controllers: [UsersController],
    providers: [UsersService, UserSubscriber]
  })
  export class UsersModule {}
```
  - 이제 로그를 활용해 entity의 이벤트들을 받을 수 있습니다.


## Multiple databases
  - TypeORM은 아래와 같이 멀티 컨넥션을 지원합니다.
```typescript
  const defaultOptions = {
    type: 'postgres',
    port: 5432,
    username: 'user',
    password: 'password',
    database: 'db',
    synchronize: true,
  };

  @Module({
    imports: [
      TypeOrmModule.forRoot({
        ...defaultOptions,
        host: 'user_db_host',
        entities: [User],
      }),
      TypeOrmModule.forRoot({
        ...defaultOptions,
        name: 'albumsConnection',
        host: 'album_db_host',
        entities: [Album],
      }),
    ],
  })
  export class AppModule {}
```
  - 이 경우 repository 및 datasource와 연동하는 과정에서도 관련 이름을 지정해서 등록해줘야 합니다.
```typescript
  @Module({
    imports: [
      TypeOrmModule.forFeature([User]),
      TypeOrmModule.forFeature([Album], 'albumsConnection'),
    ],
  })
  export class AppModule {}

  @Injectable()
  export class AlbumsService {
    constructor(
      @InjectDataSource('albumsConnection')
      private dataSource: DataSource,
      @InjectEntityManager('albumsConnection')
      private entityManager: EntityManager,
    ) {}
  }
```



----

