

---

TeamEverywhere NestJS Guide
*Updated at 2022-09-06*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
*generated with [TeamEverywhere](https://www.team-everywhere.com/)*

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Simple API
  - 자 그럼 이제 간단한 API를 만들어 봅시다.
  - 우선은 CLI 명령어를 활용해 기본 코드를 설치하겠습니다.
  - 설치 : npm i -g @nestjs/cli
  - 실행 : nest new simple-api
  - cli 명령어를 활용하여 module, controller, service 순으로 생성합니다.
  - 예 : nest g co user

## User API 만들기
  - 회원가입, 로그인, 아이디 찾기, 비밀번호 변경 등을 만들어 봅시다.
  - 상품 및 장바구니 CRUD를 만들어 봅시다.

## Swagger
  - 설치 : npm i @nestjs/swagger@5.2.1 --save
  - 설치 : npm i swagger-ui-express --save

----

