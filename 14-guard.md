

---

TeamEverywhere NestJS Guide
*Updated at 2022-09-06*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
*generated with [TeamEverywhere](https://www.team-everywhere.com/)*

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Guard

  <img src="Guards_1.png" width="1280" height="300" />

  - 자 이제 우리가 무엇인지 정확히는 모르지만 요긴하게 사용했던 guard에 대해 공부해 보겠습니다.
  - guard 클래스는 @Injectable() 데코레이터로 정의하며 CanActivate 인터페이스를 상속합니다.
  - guard는 하나의 목적만을 가집니다.
  - 전달받은 request가 해당 router에서 처리될 지 아닐지의 여부만을 결정합니다.
  - 물론 이 경우에도 middleware로 처리할 수 있으나 middleware로는 이 다음에 어떠한 route handler가 next() 메소드로 실행되야 하는 지 알 수 없습니다.
  - 하지만 guard는 ExecutionContext에 접근할 수 있기 때문에 실행환경에 따른 처리가 가능합니다. 


## Authorization guard
  - 우리가 이미 공부를 한 것처럼 authorization은 guard의 좋은 활용 예입니다.
  - 특정 라우터에 접근 권한을 설정하는 것은 guard에 가장 적합한 사용법이기 때문입니다.
  - 아래와 같이 auth.guard.ts를 만들어서 인증된 user를 받은 후 validateRequest() 함수를 만들어서 검증할 수도 있습니다.

```typescript
  import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
  import { Observable } from 'rxjs';

  @Injectable()
  export class AuthGuard implements CanActivate {
    canActivate(
      context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
      const request = context.switchToHttp().getRequest();
      return validateRequest(request);
    }
  }
```

  - 모든 guard는 canActivate()를 상속해야 하고 해당 메소드는 동기적 혹은 비동기적으로라도 true/false를 반환해야 합니다.
  - 만약 해당 guard가 false를 반환한다면 해당 request는 거절 됩니다.


## Execution context
  - canActivate()는 하나의 인자를 받는 데, ExecutionContext 인스턴스 입니다. 
  - NestJS는 Nest HTTP server-based, microservices,  WebSockets application contexts 등과 같은 여러개의 context를 제공하며 보다 용이한 개발 환경을 제공합니다.
  - 이런 컨텍스트들이 여러 상황에서 제대로 구현될 수 있도록 제공되는 것이 ExecutionContext입니다.

## Role-based authentication
  - 유적의 특정 role에 따른 guard를 구현해 봅시다.
  - 아래와 같이 간단한 guard를 구현하겠습니다.

```typescript
  import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
  import { Observable } from 'rxjs';

  @Injectable()
  export class RolesGuard implements CanActivate {
    canActivate(
      context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
      return true;
    }
  }
```

  - 우리는 권한 검증 코드를 구현해보았기 때문에 위 코드에서 부족한 것이 무엇인지 알고 있습니다.
  - 해당 코드는 ExecutionContext로부터 request를 가져오는 함수가 부족하고 true만을 반환하기 때문에, 무조건 요청을 승인할 것입니다.

## Binding guards
  - 위 가드를 적용한다면 우리가 이미 한 대로 아래와 같이 적용할 수 있습니다.
  - guard는 controller-scoped, method-scoped, global-scoped의 단계로 설정이 가능합니다.
  - 우리는 @UseGuards() 데코레이터를 활용하여 controller-scoped 가드로 적용했습니다.

```typescript
  @Controller('cats')
  @UseGuards(RolesGuard)
  export class CatsController {}
```

  - 위 코드는 RoleGuard의 타입을 적용했습니다만, 아래와 같이 인스턴스를 생성하여 적용할 수도 있습니다.

```typescript
  @Controller('cats')
  @UseGuards(new RolesGuard())
  export class CatsController {}
```

## Setting roles per handler
  - 하지만 위 코드는 상당히 단순합니다.
  - execution context를 활용하면 보다 다양한 방식으로 guard를 활용할 수도 있습니다.
  - @SetMetadata() 데코레이터를 활용하면 아래와 같이 role을 정의해줄 수 있습니다.

```typescript
  @Post()
  @SetMetadata('roles', ['admin'])
  async create(@Body() createCatDto: CreateCatDto) {
    this.catsService.create(createCatDto);
  }
```

  - 하지만 우리는 보다 나은 방식을 이미 배웠습니다.
  - 그건 바로 아래와 같이 해당 decorator를 생성하는 것이죠.

```typescript
  import { SetMetadata } from '@nestjs/common';
  export const Roles = (...roles: string[]) => SetMetadata('roles', roles);
``` 

  - 그럼 이제 아래와 같이 custom @Roles() 데코레이터를 사용할 수 있습니다.

```typescript
  @Post()
  @Roles('admin')
  async create(@Body() createCatDto: CreateCatDto) {
    this.catsService.create(createCatDto);
  }
```

  - 그럼 이제 우리가 이미 구축한 것과 비슷하게 role guard 코드에서 role을 검증하는 코드를 만들 수 있습니다.
  - ExecutionContext를 통해 현재 user 정보를 가지고 오고, 이를 matchRoles라는 메소드를 만들어서 비교 후 boolean 값을 응답하는 것입니다.

```typescript
  import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
  import { Reflector } from '@nestjs/core';

  @Injectable()
  export class RolesGuard implements CanActivate {
    constructor(private reflector: Reflector) {}

    canActivate(context: ExecutionContext): boolean {
      const roles = this.reflector.get<string[]>('roles', context.getHandler());
      if (!roles) {
        return true;
      }
      const request = context.switchToHttp().getRequest();
      const user = request.user;
      return matchRoles(roles, user.roles);
    }
  }
```

----

