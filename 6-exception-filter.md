

---

TeamEverywhere NestJS Guide
*Updated at 2022-09-06*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
*generated with [TeamEverywhere](https://www.team-everywhere.com/)*

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Exception filters
  - NestJS는 내장된 예외 처리 레이어를 제공합니다.
  - 때문에 우리가 직접 작성한 코드에서 예외 처리가 되지 않는다면, 내부 예외 처리 코드에 의해 처리됩니다.
  - 이를 통해 클라이언트는 적절한 에러 관련 response를 받을 수 있게 됩니다.
  - 이는 NestJS에 내장된 global exception filter를 통해 이루어지는데, HttpException을 관리합니다.
  - 만약 HttpException가 아닌 예외의 경우, 아래와 같이 기본 에러 JSON을 응답합니다.
  ```typescript
  {
    "statusCode": 500,
    "message": "Internal server error"
  }
  ```

## Throwing standard exceptions
  - error response를 확인하기위해 예외를 발생시켜 봅시다.
  - HttpException과 HttpStatus는 모두 @nestjs/common에서 import 할 수 있습니다.
  ```typescript
  @Get()
  async findAll() {
    throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
  }
  ```
  - 위 코드는 아래와 같이 에러 관련 json 메세지를 응답합니다.
  ```typescript
    {
      "statusCode": 403,
      "message": "Forbidden"
    }
  ```
  - 아래와 같이 HttpException의 response 부분을 변경할 수 있습니다.
  - 메세지 부분만 overriding 하기 위해서는 string을 전달하면 되고, 
  - JSON response body를 수정하고 싶다면, object를 전달하면 됩니다.
  ```typescript
    @Get()
    async findAll() {
      throw new HttpException({
        status: HttpStatus.FORBIDDEN,
        error: 'This is a custom message',
      }, HttpStatus.FORBIDDEN);
    }
  ```
  - 이렇게 하면 error 메세지가 변경된 것을 확인할 수 있습니다.
  ```typescript
  {
    "status": 403,
    "error": "This is a custom message"
  }

  ```


----

