

---

TeamEverywhere NestJS Guide
*Updated at 2022-09-06*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
*generated with [TeamEverywhere](https://www.team-everywhere.com/)*

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Provider
<img src="Components_1.png" width="1280" height="540" />

  - 프로바이더는 NestJS의 기본적인 컨셉입니다.
  - services, repositories, factories, helpers 등 다양한 클래스가 프로바이더로 취급될 수 있습니다.
  - 프로바이더의 기본적인 아이디어는 dependency injection으로 활용될 수 있다는 점입니다.
    이것은 객체들이 서로 다양한 관계를 맺을 수 있다는 것을 의미합니다.
  
## Services
  - 예제의 CatsService를 보면 @Injectable 데코레이터가 있습니다. 
  - 이 데코레이터가 붙으면 CatsService 클래스가 Nest IoC container에 의해 관리될 수 있다는 의미입니다.
  - 그로인해 CatsController의 생성자를 통해 주입할 수 있습니다.
  - private 문법을 사용함으로써, catsService를 같은 범위 내에서 사용할 수 있게 합니다.

## Dependency Injection
  - NestJS는 Angular에서 DI의 영감을 받았습니다.
  - 때문에 Angular의 DI 컨셉을 한번 살펴보겠습니다.
  - 다음과 같은 서비스가 있다고 해봅시다.
  ```typescript
    @Injectable()
    class HeroService {}
  ```
  - Angular에서는 다음과 같이 HelloService를 provider로 등록해주어 사용할 수 있게 해주어야 합니다.
  ```typescript
  @NgModule({
    declarations: [HeroListComponent]
    providers: [HeroService]
  })
  class HeroListModule {}
  ```
  - NestJS로 바꿔보면 다음과 같을 겁니다.
  ```typescript
  @Module({
    controllers: [CatsController],
    providers: [CatsService],
  })
  export class CatsModule {}
  ```
  - NestJS에서는 typescript의 type을 활용해서 생성자에 변수로 provider 클래스를 아주 쉽게 선언할 수 있습니다.
  
## Scopes
  - NestJS는 Scope라는 단위로 앱 내 life-cycle을 관리합니다.
  - Provider는 일반적으로 application life-cycle과 동기화됩니다.
  - NestJS app이 실행되면 provider 클래스들은 인스턴스화 되고, 앱이 종료되면 provider 들은 종료됩니다.
  - 물론 provider의 라이프타임을 request-scoped로 변경할 수도 있습니다.

## Custom providers
  - NestJS는 빌트인 inversion of control ("IoC") 컨테이너를 가지고 있고 provider들 관의 관계를 관리합니다.
  - 빌트인 IoC는 다양한 커스텀 프로바이더들(classes, 동기, 비동기 팩토리등)을 지원합니다.

## Optional providers
  - 설정 객체들을 다룰때에는 값이 전달되지 않을때 default 값들만 사용하는 등,
    provider들을 optional로 관리하고 싶을 수가 있습니다.
  - 
  ```typescript
  import { Injectable, Optional, Inject } from '@nestjs/common';

  @Injectable()
  export class HttpService<T> {
    constructor(@Optional() @Inject('HTTP_OPTIONS') private httpClient: T) {}
  }
  ```

## Property-based injection
   - 아래와 같이 생성자를 통하지 않고도 의존성을 주입할 수 있습니다.
   ```typescript
   import { Injectable, Inject } from '@nestjs/common';

    @Injectable()
    export class HttpService<T> {
      @Inject('HTTP_OPTIONS')
      private readonly httpClient: T;
    }
   ```

## Provider registration
  - Provider를 사용하기 위해서는 provide를 모듈에 적용해야 하고, 그리고 provider를 사용할 consumer(CatsController)를 정의해줘야 합니다.
  ```typescript
  import { Module } from '@nestjs/common';
  import { CatsController } from './cats/cats.controller';
  import { CatsService } from './cats/cats.service';

  @Module({
    controllers: [CatsController],
    providers: [CatsService],
  })
  export class AppModule {}
  ``` 
----

