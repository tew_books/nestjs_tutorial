

---

TeamEverywhere NestJS Guide
*Updated at 2022-09-06*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
*generated with [TeamEverywhere](https://www.team-everywhere.com/)*

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## TypeORM Integration
  - TypeORM 연동을 위해 NestJS는 @nestjs/typeorm package를 제공합니다.
  - 우선 MySQL을 사용하기 위한 TypeORM 모듈을 설치해보겠습니다.
  - 설치 : npm install --save @nestjs/typeorm typeorm mysql2
  - 필요한 모듈을 설치했다면 우리는 TypeOrmModule을 import 할 수 있습니다.
  ```typescript
    import { Module } from '@nestjs/common';
    import { TypeOrmModule } from '@nestjs/typeorm'; 
    @Module({
      imports: [
        TypeOrmModule.forRoot({
          type: 'mysql',
          host: 'localhost',
          port: 3306,
          username: 'root',
          password: 'root',
          database: 'test',
          entities: [],
          synchronize: true,
        }),
      ],
    })
    export class AppModule {}
  ```
  - synchronize를 true로 하는 것은 상용 서버에서는 하면 안됩니다. 개발에 따라 데이터베이스를 업데이트 하기 때문에 상용 서버의 데이터를 잃게 됩니다.
  - 위 설정에 더불어 아래와 같이 추가 설정을 할 수 있습니다.

|Properties|Description|
|-------|----------|
|retryAttempts|데이터베이스 연결 시도 횟수 (default: 10)|
|retryDelay|재연결 시도 간 딜레이(대기 시간) (default: 3000ms)|
|autoLoadEntities|True로 선언되면 엔티티들이 자동으로 로드 됩니다. (default: false)|

  - 보다 많은 설정 옵션은 아래 링크에서 확인할 수 있습니다.
  - https://typeorm.io/data-source-options

## Repository pattern
  - TypeOrm은 Repository design pattern을 지원하기 때문에, 각각의 entity는 그에 해당하는 repository를 보유합니다.
  - User entity를 Define 해보겠습니다.
  ```typescript
  import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
  @Entity()
  export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column({ default: true })
    isActive: boolean;
  }
  ```

## Entity
  - Entity는 데이터베이스와 연결되어 테이블을 구성하는 클래스입니다.
  - 기본적인 Entity는 컬럼과 릴레이션들로 구성되어 있습니다. 
  - 각각의 Entity는 반드시 primary column을 보유하고 있어야 합니다.
  - 또한 모든 Entity는 datasource option에 포함되어 있어야 합니다.
  ```typescript
  import { DataSource } from "typeorm"
  import { User } from "./entity/User"

  const myDataSource = new DataSource({
      type: "mysql",
      host: "localhost",
      port: 3306,
      username: "test",
      password: "test",
      database: "test",
      entities: [User],
  })
  ```
  - 혹은 특정 경로에 있는 폴더를 모두 가져올 수 있습니다.
  ```typescript
  import { DataSource } from "typeorm"
  const dataSource = new DataSource({
      type: "mysql",
      host: "localhost",
      port: 3306,
      username: "test",
      password: "test",
      database: "test",
      entities: ["entity/*.js"],
  })
  ```
  - UserModule을 아래와 같이 수정해주어 어떤 repository가 현재 모듈에 등록되는 지 결정합니다.
  ```typescript
    import { Module } from '@nestjs/common';
    import { TypeOrmModule } from '@nestjs/typeorm';
    import { UsersService } from './users.service';
    import { UsersController } from './users.controller';
    import { User } from './user.entity';

    @Module({
      imports: [TypeOrmModule.forFeature([User])],
      providers: [UsersService],
      controllers: [UsersController],
    })
    export class UsersModule {}
  ```
  - 이제 우리는 UsersRepository를 가져와 UsersService에서 사용할 수 있습니다.
  ```typescript
    import { Injectable } from '@nestjs/common';
    import { InjectRepository } from '@nestjs/typeorm';
    import { Repository } from 'typeorm';
    import { User } from './user.entity';

    @Injectable()
    export class UsersService {
      constructor(
        @InjectRepository(User)
        private usersRepository: Repository<User>,
      ) {}

      findAll(): Promise<User[]> {
        return this.usersRepository.find();
      }

      findOne(id: string): Promise<User> {
        return this.usersRepository.findOneBy({ id });
      }

      async remove(id: string): Promise<void> {
        await this.usersRepository.delete(id);
      }
    }
  ```
  - TypeOrmModule.forFeature를 통해 import한 repository를 다른 모듈에서도 사용할 수 있게 하려면 아래와 같이 re-export 하여야 합니다.
  ```typescript
    import { Module } from '@nestjs/common';
    import { TypeOrmModule } from '@nestjs/typeorm';
    import { User } from './user.entity';

    @Module({
      imports: [TypeOrmModule.forFeature([User])],
      exports: [TypeOrmModule]
    })
    export class UsersModule {}
  ```
   

## @PrimaryColumn()
   - primary column을 만들어 냅니다.
   ```typescript
    import { Entity, PrimaryColumn } from "typeorm"
    @Entity()
    export class User {
        @PrimaryColumn()
        id: number
    }
   ```

## @PrimaryGeneratedColumn()
  - Auto increment가 적용 된 primary column을 만들어 냅니다. 
  - 해당 column은 int여야 하고 save 함수를 호출할 때 id의 값을 설정할 필요가 없습니다.
  ```typescript
    import { Entity, PrimaryGeneratedColumn } from "typeorm"
    @Entity()
    export class User {
        @PrimaryGeneratedColumn()
        id: number
    }
  ```

## @PrimaryGeneratedColumn("uuid")
  - Unique string id인 uuid를 활용하여 자동으로 primary column을 만들어 냅니다.
  - save 함수를 호출할 때 id의 값을 설정할 필요가 없습니다.
  ```typescript
    import { Entity, PrimaryGeneratedColumn } from "typeorm"

    @Entity()
    export class User {
        @PrimaryGeneratedColumn("uuid")
        id: string
    }
  ```

## Primary Column의 활용
  - 아래와 같이 여러개의 primary column을 설정할 수도 있습니다.
  ```typescript
    import { Entity, PrimaryColumn } from "typeorm"
    @Entity()
    export class User {
        @PrimaryColumn()
        firstName: string

        @PrimaryColumn()
        lastName: string
    }

  ```  

## enum column type
  - Enum 타입은 postgres와 mysql에 지원 됩니다.
  ```typescript
  export enum UserRole {
      ADMIN = "admin",
      EDITOR = "editor",
      GHOST = "ghost",
    }

    @Entity()
    export class User {
        @PrimaryGeneratedColumn()
        id: number

        @Column({
            type: "enum",
            enum: UserRole,
            default: UserRole.GHOST,
        })
        role: UserRole
    }
  ```
  - 혹은 아래와 같이 array를 활용하여 설정할 수도 있습니다.
  ```typescript
    export type UserRoleType = "admin" | "editor" | "ghost",

    @Entity()
    export class User {

        @PrimaryGeneratedColumn()
        id: number;

        @Column({
            type: "enum",
            enum: ["admin", "editor", "ghost"],
            default: "ghost"
        })
        role: UserRoleType
    }
  ```

## set column type
  - mariadb와 mysql에서 지원 됩니다.
  ```typescript
    export enum UserRole {
      ADMIN = "admin",
      EDITOR = "editor",
      GHOST = "ghost",
    }

    @Entity()
    export class User {
        @PrimaryGeneratedColumn()
        id: number

        @Column({
            type: "set",
            enum: UserRole,
            default: [UserRole.GHOST, UserRole.EDITOR],
        })
        roles: UserRole[]
    }
  ```
   - 아래와 같이 array를 사용해서 구축할 수도 있습니다.
  ```typescript
    export type UserRoleType = "admin" | "editor" | "ghost",

    @Entity()
    export class User {

        @PrimaryGeneratedColumn()
        id: number;

        @Column({
            type: "set",
            enum: ["admin", "editor", "ghost"],
            default: ["ghost", "editor"]
        })
        roles: UserRoleType[]
    }
  ```

## simple-array column type
 - 원시 타입의 array를 저장하려면 아래와 같이 만들 수 있습니다.
 ```typescript
    // entity.ts
    @Entity()
    export class User {
        @PrimaryGeneratedColumn()
        id: number

        @Column("simple-array")
        names: string[]
    }

    // entity 선언 코드
    const user = new User()
    user.names = ["Alexander", "Alex", "Sasha", "Shurik"]
 ```

## simple-json column type
  - 아래와 같이 작업하면 database에 JSON.stringify된 데이터가 저장되게 됩니다.
  ```typescript
  // entity.ts
  @Entity()
  export class User {
      @PrimaryGeneratedColumn()
      id: number

      @Column("simple-json")
      profile: { name: string; nickname: string }
  }
  // entity 선언 코드
  const user = new User()
  user.profile = { name: "John", nickname: "Malkovich" }
  ```

## Entity inheritance
   - Entity를 상속하여 중복 데이터를 없앨 수 있습니다.
   - 아래와 같이 Entity 클래스들이 있다고 가정해 봅시다. 
  ```typescript
    @Entity()
    export class Photo {
        @PrimaryGeneratedColumn()
        id: number

        @Column()
        title: string

        @Column()
        description: string

        @Column()
        size: string
    }

    @Entity()
    export class Question {
        @PrimaryGeneratedColumn()
        id: number

        @Column()
        title: string

        @Column()
        description: string

        @Column()
        answersCount: number
    }

    @Entity()
    export class Post {
        @PrimaryGeneratedColumn()
        id: number

        @Column()
        title: string

        @Column()
        description: string

        @Column()
        viewCount: number
    }
  ```
   - 위 entity 클래스들은 id, title, description이 중복 적용 되어 있습니다.
   - 중복을 줄이기 위해 코드를 아래와 같이 base class인 Content class를 만들어서 수정해보겠습니다.
   ```typescript
   export abstract class Content {
      @PrimaryGeneratedColumn()
      id: number

      @Column()
      title: string

      @Column()
      description: string
    }
    @Entity()
    export class Photo extends Content {
        @Column()
        size: string
    }

    @Entity()
    export class Question extends Content {
        @Column()
        answersCount: number
    }

    @Entity()
    export class Post extends Content {
        @Column()
        viewCount: number
    }
   ```

## Adjacency list
  - 아래와 같이 인접 리스트를 구현할 수도 있습니다.
  ```typescript
  import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToOne,
    OneToMany,
  } from "typeorm"

  @Entity()
  export class Category {
      @PrimaryGeneratedColumn()
      id: number

      @Column()
      name: string

      @Column()
      description: string

      @ManyToOne((type) => Category, (category) => category.children)
      parent: Category

      @OneToMany((type) => Category, (category) => category.parent)
      children: Category[]
  }
  ```


## Closure table을 사용하면 데이터를 구분하여 저장할 수 있습니다.
  - Hierachical data 모델에 대해 궁금한 분은 아래 링크를 참고하시면 됩니다.
  - https://www.slideshare.net/billkarwin/models-for-hierarchical-data
```typescript
  import {
      Entity,
      Tree,
      Column,
      PrimaryGeneratedColumn,
      TreeChildren,
      TreeParent,
      TreeLevelColumn,
  } from "typeorm"

  @Entity()
  @Tree("closure-table")
  export class CategoryClosueTable {
      @PrimaryGeneratedColumn()
      id: number

      @Column()
      name: string

      @Column()
      description: string

      @TreeChildren()
      children: CategoryClosueTable[]

      @TreeParent()
      parent: CategoryClosueTable

      @TreeLevelColumn()
      level: number
  }
```



----

