

---

TeamEverywhere NestJS Guide
*Updated at 2022-09-06*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
*generated with [TeamEverywhere](https://www.team-everywhere.com/)*

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Controller
<img src="Controllers_1.png" width="640" height="270" />

  - 컨트롤러는 어플리케이션의 특정 request를 처리하기 위해 존재합니다.
  - NestJS의 routing 메커니즘이 특정 controller가 특정 request를 받을 수 있도록 처리해줍니다.
  - 컨트롤러에서는 Decorator를 사용하여 필요한 metadata들을 보조하고 Nest가 routing map을 생성할 수 있도록 합니다.
  
## Routing
  - @Controller Decorator를 활용하여 cats를 prefix로 붙일 수 있습니다.
  - @Get Decorator와 같이 http method를 설정할 수 있습니다.
  ```typescript
  import { Controller, Get } from '@nestjs/common';
  @Controller('cats')
  export class CatsController {
    @Get('profile')
    findAll(): string {
      return 'This action returns all cats';
    }
  }
  ```

## Request Object
  - 아래와 같이 express의 request를 활용하여 쿼리 정보 등을 받아올 수도 있습니다.
  - 일반적으로는 NestJS에서 제공하는 @Body(), @Query() 데코레이터를 사용하면 되지만 아래와 같은 Decorator들을 자유롭게 사용할 수 있습니다.  

|데코레이터|사용|
|-------|---|
|@Request(), @Req()|req|
|@Response(), @Res()*|res|
|@Next()|next|
|@Session()|req.session|
|@Param(key?: string)	req.params|req.params[key]|
|@Body(key?: string)	req.body|req.body[key]|
|@Query(key?: string)	req.query|req.query[key]|
|@Headers(name?: string)	req.headers|req.headers[name]|
|@Ip()|req.ip|
|@HostParam()|req.hosts|    
	
	
  ```typescript
  import { Controller, Get, Req } from '@nestjs/common';
  import { Request } from 'express';

  @Controller('cats')
  export class CatsController {
    @Get()
    findAll(@Req() request: Request): string {
      console.log(request)
      return 'This action returns all cats';
    }
  }
  ```

## Status code
  ```typescript
    @Post()
    @HttpCode(204)
    create() {
      return 'This action adds a new cat';
    }
  ```

## Header
  ```typescript
    @Post()
    @Header('Cache-Control', 'none')
    create() {
      return 'This action adds a new cat';
    }
  ```

## Redirection
  - redirect 데코레이터는 url과 statuscode를 받습니다.
  - status code의 기본값은 301(found) 입니다.
  ```typescript
    @Get()
    @Redirect('https://nestjs.com', 301)

  ```

## Route Parameter
  - 경로로 다이나믹한 데이터를 받고자 할때는 다음과 같이 할 수 있습니다.
  ```typescript
    @Get(':id')
    findOne(@Param() params): string {
      console.log(params.id);
      return `This action returns a #${params.id} cat`;
    }

  ```

----

