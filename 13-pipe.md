

---

TeamEverywhere NestJS Guide
*Updated at 2022-09-06*

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
*generated with [TeamEverywhere](https://www.team-everywhere.com/)*

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Pipes

  <img src="Pipe_1.png" width="1280" height="540" />

  - 파이프 클래스는 @Injectable() 데코레이터를 사용해서 PipeTransform 인터페이스를 상속합니다.
  - 예를 들어 관리자 유저는 데이터의 생성, 조회, 수정, 삭제 등을 모두 할 수 있지만, 비 관리자 유저는 읽기만 가능한 것이죠.
  - 파이프는 아래와 같이 전형적으로 사용되는 두가지 케이스를 가지고 있습니다.
    - transformation : input data를 원하는 형태로 변경합니다.
    - validation : input data를 검증하고 통과시키거나 에러를 발생시킵니다.
  - 두가지 케이스에서 모두 controller router handler에서 처리 되는 arguments를 관리합니다.
  - NestJS는 각 메소드가 호출되기 전에 pipe를 실행시켜 관리할 수 있게 합니다.
  - 때문에 route handler는 변경되거나(transformation), 검증(validation)된 arguments(인수)를 받을 수 있습니다.
  - Nest는 여러가지 built-in pipe를 가지고 있고 혹은 custom pipe를 선언하여 사용할 수도 있습니다.


## Built-in pipes
  - NestJS는 다음과 같이 built-in pipe를 제공합니다.
  - ValidationPipe
  - ParseIntPipe
  - ParseFloatPipe
  - ParseBoolPipe
  - ParseArrayPipe
  - ParseUUIDPipe
  - ParseEnumPipe
  - DefaultValuePipe
  - ParseFilePipe
  - 이 중 ParseIntPipe는 transformation에 해당하는 케이스로 해당 메소드의 파라미터가 javascript의 integer로 변환됨을 보장합니다.

## Binding pipes
  - pipe를 사용하기 위해서는 아래와 같이 파라미터롸 함께 묶어줘야 합니다.

```typescript
    @Get(':user_idx')
    findIdx(
      @Param('user_idx', new ParseIntPipe()) user_idx: number,
    ): Promise<User> {
      console.log('id : ', user_idx)
      // get by ID logic
      return this.usersService.findIdx(user_idx)
    }
```

  - 위 코드는 findIdx로 받는 파라미터가 number임을 보장하고, route handler가 호출되기 전에 exception을 일으킵니다.
  - localhost:3000/users/asdf 로 호출하고 에러 문구를 확인해 보겠습니다.
  - 위 코드는 ParseIntPipe 클래스는 넣었고 instance(객체)를 추가하지는 않았습니다.
  - 클래스에서 객체를 생성하고 의존성을 주입할 필요가 없는데, 그 자리에서 옵션을 설정하기에도 편합니다.

```typescript
    @Get(':user_idx')
    findIdx(
      @Param('user_idx', new ParseIntPipe({ errorHttpStatusCode: HttpStatus.NOT_ACCEPTABLE })) user_idx: number,
    ): Promise<User> {
      console.log('id : ', user_idx)
      // get by ID logic
      return this.usersService.findIdx(user_idx)
    }
```

  - 위와 같이 고치면 옵션에 따라 변경 된 에러 메세지 또한 확인할 수 있습니다.
  - query string에도 아래와 같이 추가할 수 있습니다.

```typescript
    @Get('query')
    async findOne(@Query('user_idx', ParseIntPipe) user_idx: number) {
      return this.usersService.findIdx(user_idx)
    }
```

## Custom pipes
  - NestJS는 빌트인 ValidationPipe를 제공하지만 한번 커스텀 validationPipe를 만들어 보겠습니다.

```typescript

    import { PipeTransform, Injectable, ArgumentMetadata } from '@nestjs/common';

    @Injectable()
    export class ValidationPipe implements PipeTransform {
      transform(value: any, metadata: ArgumentMetadata) {
        return value;
      }
    }
```

  - 위 코드는 단순히 input value와 동일한 밸류를 return 합니다.
  - 모든 pipesms transform() 메소드를 상속받아야 되는데 value와 metadata 파라미터를 가지고 있습니다.
  - value는 route handler전에 전달 받는 인자들입니다.
  - metadata는 인자의 metadata입니다.
  - metadat의 객체는 아래와 같이 이루어져 있습니다.

```typescript
  export interface ArgumentMetadata {
    type: 'body' | 'query' | 'param' | 'custom';
    metatype?: Type<unknown>;
    data?: string;
  }
```

  - type은 인자들이 body, query 등의 형식 중 무엇인지를 나타냅니다.
  - metatype은 String과 같은 타입을 의미합니다.
  - data는 @Body('string')와 같이 데코레이터로 전달되는 값의 형식을 의미합니다.


## Schema based validation
  - 자 이제 custom validation pipe를 만들어 봅시다.
  - 우선 우리는 상품 등록시 들어오는 데이터가 적합한 body 데이터인지 검증하고 싶습니다.
  - 우리의 경우에는 good_name과 price가 제대로 들어오는 지만 확인하면 될 것 같습니다.
  - 물론 해당 데이터의 검증을 router handler에서 할 수도 있습니다만, single responsibility rule (SRP)을 깨는 경우기 때문에 이상적이지 않습니다.
  - SRP란 모든 모듈과 클래스, 함수 등은 단 하나의 목적/책임만 가져야 한다는 철학입니다.
  - 또 하나의 방법으로 validator class를 만드는 게 있을 텐데 항상 해당 클래스를 호출해야 하기 때문의 누락의 위험이 있습니다.
  - 혹은 middleware를 만들 수 있으나 application 전체에서 사용될 수 있는 middleware를 만드는 게 불가능한데,
    이는 실행 컨텍스트를 알 수 없기 때문입니다. (실행 컨텍스트 : 실행 가능한 코드가 실행되기 위해 필요한 환경)


## Object schema validation
  - 우리는 joi library를 활용하여 schema를 만들고 검증하도록 하겠습니다.

```bash
  $ npm install --save joi
  $ npm install --save-dev @types/joi
```

  - 그리고 아래와 같은 validation pipe 코드를 만들겠습니다.
  - 생성자는 우리가 전달하는 schema 데이터를 받습니다.
  - schema.validate() 코드가 생성자를 통해 전달하는 데이터를 검증합니다.

```typescript
  import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException } from '@nestjs/common';
  import { ObjectSchema } from 'joi';

  @Injectable()
  export class JoiValidationPipe implements PipeTransform {
    constructor(private schema: ObjectSchema) {}

    transform(value: any, metadata: ArgumentMetadata) {
      const { error } = this.schema.validate(value);
      if (error) {
        throw new BadRequestException('Validation failed');
      }
      return value;
    }
  }

```


## Binding validation pipes
  - 이번에는 UsePipes 데코레이터를 활용해서 pipe를 적용하겠습니다.
  - 우선 JoiValidationPipe 인스턴스를 생성합니다.
  - Joi schema를 생성자에 전달합니다.
  - 해당 pipe를 메소드에 적용하는 순입니다.

```typescript
  @Post('register')  
  @UsePipes(
    new JoiValidationPipe(
      joi.object().keys({ good_name: joi.string(), price: joi.number() }),
    ),
  )
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles(Role.Admin)
  create2(@Body() createGoodDto: CreateGoodDto): Promise<InsertResult> {
    return this.goodsService.create(createGoodDto);
  }
```

  - 이제 바디 데이터를 타입에 맞지 않게 보내면 원래 internal server 에러를 반환하던 것이 validation 에러를 응답하는 것을 확인할 수 있습니다.

----

